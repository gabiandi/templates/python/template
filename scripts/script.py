#!/usr/bin/env python3

"""Hello world example script."""
import sys
from template.submodule.module import ModuleExample


def main():
    print('Hello world from script example')
    ModuleExample.progress()
    return 0


if __name__ == '__init__':
    sys.exit(main)
