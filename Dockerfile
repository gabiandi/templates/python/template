FROM python:3.12

USER root

COPY . /temp/template

RUN pip3 install -U /temp/template

CMD [ "template_hello_word_example" ]
