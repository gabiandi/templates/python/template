"""Module example."""
import time
from progressbar import Bar, ProgressBar, SimpleProgress, AdaptiveETA


def progress(iterable):
    """Barra de progreso."""
    pbar = ProgressBar(widgets=[SimpleProgress(), Bar(), AdaptiveETA()])
    return pbar(iterable)


class ModuleExample:
    """Module example class."""

    def __init__(self):
        """Module example init."""
        print('Hello world from module')

    @classmethod
    def progress(cls):
        print('Loading:')
        for _ in progress(range(100)):
            time.sleep(0.1)
