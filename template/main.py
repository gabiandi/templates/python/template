"""Main script from template module example."""
import sys
from .submodule.module import ModuleExample


def main():
    """Main function of template module.

    Returns:
        int: result of execute
    """
    print('Hello world from main')
    ModuleExample()
    ModuleExample.progress()
    return 0


if __name__ == '__main__':
    """Execute point of file."""
    sys.exit(main())
