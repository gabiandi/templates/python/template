"""Tests package"""
import unittest
import os
from flake8.api.legacy import get_style_guide


class TestPackage(unittest.TestCase):
    """Style flake8 testing."""

    def _get_python_filepaths(self):
        """Get Python files in project."""
        python_paths = []
        for root in ['template', 'tests']:
            for dirpath, _, filenames in os.walk(root):
                for filename in filenames:
                    if filename.endswith('.py'):
                        python_paths.append(os.path.join(dirpath, filename))
        return python_paths

    def test_flake8(self):
        """Test flake8 rules."""
        python_filepaths = self._get_python_filepaths()
        style_guide = get_style_guide()
        report = style_guide.check_files(python_filepaths)
        self.assertEqual(report.total_errors, 0)
