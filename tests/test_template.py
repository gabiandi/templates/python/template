"""Test base."""
import unittest
import template
from template import submodule
from template.test.test import TestingClass


class TestSum(unittest.TestCase):
    """Test sum for app."""

    def test_alive(self):
        """Test sum function."""
        testing_class = TestingClass()
        a = 1
        b = 2
        c = testing_class.sum_test(a, b)
        self.assertEqual(a + b, c)

    def test_init(self):
        """Test __init__."""
        self.assertEqual(template.functions(), 0)
        self.assertEqual(submodule.functions(), 0)
