######################
Template documentation
######################

Write ``template`` module documentation. This page no component autodoc plugin.

.. toctree::
    :caption: Documentation module
    :hidden:

    template/index

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
