#########
submodule
#########

.. toctree::
    :caption: Files
    :maxdepth: 1

    module

.. automodule:: template.submodule
    :members:
    :undoc-members:
    :show-inheritance:
