########
template
########

.. toctree::
    :caption: Submodules
    :maxdepth: 1

    test/index
    submodule/index

.. toctree::
    :caption: Files
    :maxdepth: 1

    main

.. automodule:: template
    :members:
    :undoc-members:
    :show-inheritance:
